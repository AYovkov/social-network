# Social Network
>A network of social interactions and personal relationships. The Social Network allows like-minded individuals to be in touch with each other and be up to date with the latest events among their friends.

### [**Link to our project board**](https://trello.com/b/Mt93B5ik/social-network-web-app)

## Table of contents
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [API](#api)

## Screenshots
#### Guests Page (Homepage)
![](./Images/GuestPage.png)
#### Search social accounts by first name, last name and email address
![](./Images/SearchUsers.png)
#### Logged User
![](./Images/Profile_NewsFeed.png)
#### Friend Requests
![](./Images/Friend_Requests.png)
#### Settings - General
![](./Images/Settings_General.png)
#### Settings - Info
![](./Images/Settings_Info.png)
#### Settings - Password
![](./Images/Settings_Password.png)
#### Send Friend Request (Add Friend)
![](./Images/Add_Friend.png)
#### Private Profile
![](./Images/Private_Profile.png)
#### Swagger - API
![](./Images/swagger.png)

## Technologies
* ASP.NET Core - 3.1
* Entity Framework Core - 3.1
* MS SQL Server
* Bootstrap - 4.3.1
* jQuery - 3.5.1

## Setup
// TODO
## Features
* Infinite scrolling
* Email confirmation link after registration
* Password reset
## API

* Users

```sh
GET   /api/Users/{id}
GET   /api/Users/friends
```

* Posts 

```sh
GET    /api/Posts/newsfeed
POST   /api/Posts
```