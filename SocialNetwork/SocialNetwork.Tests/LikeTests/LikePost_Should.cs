﻿namespace SocialNetwork.Tests.LikeTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.LikeService;
    using System;

    [TestClass]
    public class LikePost_Should
    {
        [TestMethod]
        public void CreateLikeForPost_Success()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(CreateLikeForPost_Success));
            var date = new Mock<IDateTimeProvider>();

            var user = new User
            {
                Id = Guid.Parse("10ee1113-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var post = new Post
            {
                Id = 10,
                Content = "Test for a post",
                UserId = Guid.Parse("10ee1113-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new LikeService(assertContext);
                var result = sut.LikeAsync(post.Id,user.Id);
                var count = sut.GetVotes(10);

                Assert.AreEqual(1, count);
            }
        }
    }
}
