﻿namespace SocialNetwork.Tests.PhotosTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class AddPhoto_Should
    {
        [TestMethod]
        public void AddPhoto_Success()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(AddPhoto_Success));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var photoDTO = new PhotoDTO
            {
                Id = 1,
                PhotoName = "testPhoto.png"
            };

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PhotoService(assertContext, mapper);

                var addPhoto = sut.AddPhoto(photoDTO).Result;

                var actualName = addPhoto.PhotoName;
                var actualId = addPhoto.Id;

                Assert.AreEqual("testPhoto.png", actualName);
                Assert.AreEqual(1, actualId);
            }
        }
    }
}
