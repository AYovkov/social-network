﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialNetwork.Data;
using SocialNetwork.Services.FriendRequestService;
using SocialNetwork.Services.UserFriend;
using SocialNetwork.Services.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Tests.FriendRequestsTests
{
    [TestClass]
    public class FriendRequestExist_Should
    {
        [TestMethod]
        public void ReturnTrue_When_FriendRequestExists()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnTrue_When_FriendRequestExists));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.Exists(firstUser.Id, secondUser.Id);

                Assert.AreEqual(true, act);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_FriendRequestDoesNotExists()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnFalse_When_FriendRequestDoesNotExists));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.Exists(firstUser.Id, secondUser.Id);

                Assert.AreEqual(false, act);
            }
        }
    }
}
