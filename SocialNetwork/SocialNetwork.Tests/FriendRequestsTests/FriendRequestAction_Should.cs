﻿namespace SocialNetwork.Tests.FriendRequestsTests
{

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System.Linq;

    [TestClass]
    public class FriendRequestAction_Should
    {
        [TestMethod]
        public void ReturnValidMessage_When_Accept()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnValidMessage_When_Accept));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            var action = "accept";

            using(var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.FriendRequestAction(firstUser.Id, secondUser.Id, action);

                Assert.AreEqual("Accepted", act.Result);
                Assert.AreEqual(FriendRequestStatus.Accepted, assertContext.FriendRequests.First().FriendRequestStatus);
            }
        }

        [TestMethod]
        public void ReturnValidMessage_When_Decline()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnValidMessage_When_Decline));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            var action = "decline";

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.FriendRequestAction(firstUser.Id, secondUser.Id, action);

                Assert.AreEqual("Declined", act.Result);
                Assert.AreEqual(0, assertContext.FriendRequests.Count());
            }
        }
    }
}
