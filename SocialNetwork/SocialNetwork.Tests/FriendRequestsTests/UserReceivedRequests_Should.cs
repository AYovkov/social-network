﻿namespace SocialNetwork.Tests.FriendRequestsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [TestClass]
    public class UserReceivedRequests_Should
    {
        [TestMethod]
        public void ReturnCorrectReceivedRequestCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectReceivedRequestCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();
            var secondFriendRequest = SocialNetworkUtility.GetPendingFriendRequest().Skip(1).First();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.FriendRequests.Add(secondFriendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.UserReceivedRequests(secondUser.Id, 5);

                Assert.AreEqual(2, act.Result.Count());
            }
        }
    }
}
