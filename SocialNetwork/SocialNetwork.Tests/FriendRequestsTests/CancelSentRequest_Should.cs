﻿namespace SocialNetwork.Tests.FriendRequestsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [TestClass]
    public class CancelSentRequest_Should
    {
        [TestMethod]
        public void ReturnCorrectFriendRequestCount_When_CancelingRequest()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectFriendRequestCount_When_CancelingRequest));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(true);

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.CancelSentRequest(firstUser.Id, secondUser.Id);

                Assert.AreEqual(0, assertContext.FriendRequests.Count());
            }
        }

        [TestMethod]
        public void Throw_When_User_DoesNotExist()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Throw_When_User_DoesNotExist));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CancelSentRequest(firstUser.Id, secondUser.Id));
            }
        }
    }
}
