﻿namespace SocialNetwork.Tests
{
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using Data.Models.Enums;
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.Identity;
    using Moq;
    using Services.EntityDTOs;
    using SocialNetwork.Web.MappingProfiles;

    public class SocialNetworkUtility
    {
        public static DbContextOptions<SocialNetworkDbContext> GetOptions(string database)
        {
            return new DbContextOptionsBuilder<SocialNetworkDbContext>()
                .UseInMemoryDatabase(database)
                .Options;
        }

        public static MapperConfiguration MapperConfiguration()
        {
            return new MapperConfiguration(opts =>
            {
                opts.AddProfile(typeof(MapperProfile));
            });
        }

        public static IEnumerable<User> GetUsers()
        {
            return new User[]
            {
                new User
                {
                    Id = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    FirstName = "TestUser1Fn",
                    LastName = "TestUser1Ln",
                    Email = "TestUser1@yahoo.com",
                    DateOfBirth = new DateTime(1995, 5, 20),
                    Gender = Gender.Male,
                    Bio = "Test1 Biography",
                    JobTitle = "Test1 Job Title",
                    IsPrivate = false,
                    PhotoName = "profilePicture.png",
                    Country = "France",
                    IsDeleted = false,
                    Friends = new List<UserFriend>(),
                    Posts = new List<Post>(),
                    FriendRequestReceived = new List<FriendRequest>()
                },
                new User
                {
                    Id = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    FirstName = "TestUser2Fn",
                    LastName = "TestUser2Ln",
                    Email = "TestUser2@yahoo.com",
                    DateOfBirth = new DateTime(1995, 5, 20),
                    Gender = Gender.Male,
                    Bio = "Test2 Biography",
                    JobTitle = "Test2 Job Title",
                    IsPrivate = false,
                    PhotoName = "profilePicture.png",
                    Country = "France",
                    IsDeleted = false,
                    Friends = new List<UserFriend>(),
                    Posts = new List<Post>(),
                    FriendRequestReceived = new List<FriendRequest>()
                },
                new User
                {
                    Id = Guid.Parse("e206efe4-8b2a-4dd1-b5ae-3be7a33ba35a"),
                    FirstName = "TestUser3Fn",
                    LastName = "TestUser3Ln",
                    Email = "TestUser3@yahoo.com",
                    DateOfBirth = new DateTime(1998, 8, 21),
                    Gender = Gender.Male,
                    Bio = "Test3 Biography",
                    JobTitle = "Test3 Job Title",
                    IsPrivate = false,
                    PhotoName = "profilePicture.png",
                    Country = "Italy",
                    IsDeleted = false,
                    Friends = new List<UserFriend>(),
                    Posts = new List<Post>(),
                    FriendRequestReceived = new List<FriendRequest>()
                },
                new User
                {
                    Id = Guid.Parse("15b53e59-6616-4a37-a582-6905e23fb84a"),
                    FirstName = "TestUser4Fn",
                    LastName = "TestUser4Ln",
                    Email = "TestUser4@yahoo.com",
                    DateOfBirth = new DateTime(1993, 10, 15),
                    Gender = Gender.Male,
                    Bio = "Test4 Biography",
                    JobTitle = "Test4 Job Title",
                    IsPrivate = false,
                    PhotoName = "profilePicture.png",
                    Country = "Belgium",
                    IsDeleted = false,
                    Friends = new List<UserFriend>(),
                    Posts = new List<Post>(),
                    FriendRequestReceived = new List<FriendRequest>()
                },
                new User
                {
                    Id = Guid.Parse("6e8e71c6-3582-45f0-ad5d-82cf7361f38f"),
                    FirstName = "TestUser45Fn",
                    LastName = "TestUser5Ln",
                    Email = "TestUser5@yahoo.com",
                    DateOfBirth = new DateTime(1993, 10, 15),
                    Gender = Gender.Male,
                    Bio = "Test5 Biography",
                    JobTitle = "Test5 Job Title",
                    IsPrivate = false,
                    PhotoName = "profilePicture.png",
                    Country = "Belgium",
                    IsDeleted = false,
                    Friends = new List<UserFriend>(),
                    Posts = new List<Post>(),
                    FriendRequestReceived = new List<FriendRequest>()
                }
            };
        }

        public static IEnumerable<UserFriend> GetUserFriends()
        {
            return new UserFriend[]
            {
                new UserFriend
                {
                    UserId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    FriendId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f")
                },
                new UserFriend
                {
                    UserId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    FriendId = Guid.Parse("e206efe4-8b2a-4dd1-b5ae-3be7a33ba35a")
                },
                new UserFriend
                {
                    UserId = Guid.Parse("15b53e59-6616-4a37-a582-6905e23fb84a"),
                    FriendId = Guid.Parse("e206efe4-8b2a-4dd1-b5ae-3be7a33ba35a")
                }
            };
        }

        public static IEnumerable<FriendRequest> GetPendingFriendRequest()
        {
            return new FriendRequest[]
            {
                new FriendRequest
                {
                    SenderId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    ReceiverId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    FriendRequestStatus = FriendRequestStatus.Pending
                },
                new FriendRequest
                {
                    SenderId = Guid.Parse("e206efe4-8b2a-4dd1-b5ae-3be7a33ba35a"),
                    ReceiverId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    FriendRequestStatus = FriendRequestStatus.Pending
                }
            };
        }

        public static IEnumerable<FriendRequest> GetAcceptedFriendRequest()
        {
            return new FriendRequest[]
            {
                new FriendRequest
                {
                    SenderId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    ReceiverId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                },
                new FriendRequest
                {
                    SenderId = Guid.Parse("e206efe4-8b2a-4dd1-b5ae-3be7a33ba35a"),
                    ReceiverId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                }
            };
        }

        public static IEnumerable<Post> GetPosts()
        {
            return new Post[]
            {
                new Post
                {
                    Id = 1,
                    Content = "TestPost1",
                    UserId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    AccessStatus = AccessStatus.Public
                },
                new Post
                {
                    Id = 2,
                    Content = "TestPost2",
                    UserId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    AccessStatus = AccessStatus.Public
                },
                new Post
                {
                    Id = 3,
                    Content = "TestPost3",
                    UserId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    AccessStatus = AccessStatus.Private
                },
                new Post
                {
                    Id = 4,
                    Content = "TestPost4",
                    UserId = Guid.Parse("2dfdbecf-5229-4ccf-b79b-c52782f2278f"),
                    AccessStatus = AccessStatus.Private
                },
                new Post
                {
                    Id = 5,
                    Content = "TestPost5",
                    UserId = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                    AccessStatus = AccessStatus.Public
                },
            };
        }

        public static Mock<UserManager<User>> GetMockUserManager()
        {
            var userStoreMock = new Mock<IUserStore<User>>();
            return new Mock<UserManager<User>>(
                userStoreMock.Object, null, null, null, null, null, null, null, null);
        }
    }
}
