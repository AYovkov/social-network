﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class GetPost_Should
    {
        [TestMethod]
        public void GetPost_Success()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetPost_Success));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("53ee8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 25,
                Content = "Test for a post",
                UserId = Guid.Parse("53ee8e13-16ce-4168-8c30-12e244c4497e")
            };
            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var createOne = sut.AddPost(postOne);
                var get = sut.GetPost(25);
                var actual = get.Result;

                Assert.AreEqual(postOne.Content, actual.Content);
            }
        }
        [TestMethod]
        public void GetPost_ThrowException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetPost_ThrowException));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var get = sut.GetPost(111);
                var actual = get.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null.)";
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
