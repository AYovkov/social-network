﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Linq;

    [TestClass]
    public class GetPostsForUsers_Should
    {
        [TestMethod]
        public void Return_CorrectCount_For_GetPostsForUsers()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectCount_For_GetPostsForUsers));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("21ee8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 21,
                Content = "Test for a post",
                UserId = Guid.Parse("21ee8e13-16ce-4168-8c30-12e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var createOne = sut.AddPost(postOne);
                var cut = sut.GetPostsForUsers(userOne.Id);

                var actualCount = cut.Result.Count();

                Assert.AreEqual(1, actualCount);
            }
        }
    }
}
