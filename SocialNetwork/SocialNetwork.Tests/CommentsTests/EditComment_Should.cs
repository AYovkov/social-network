﻿namespace SocialNetwork.Tests.CommentsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.CommentService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;

    [TestClass]
    public class EditComment_Should
    {
        [TestMethod]
        public void Succsesfully_Edit_Comment_For_Post()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Edit_Comment_For_Post));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("20ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var post = new Post
            {
                Id = 23,
                Content = "Test for a post"
            };

            var comment = new Comment
            {
                Id = 23,
                Content = "Test for a comment",
                PostId = 23,
                UserId = Guid.Parse("20ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            var commentDTO = new CommentDTO
            {
                Id = 23,
                Content = "Comment Edited",
                PostId = 23,
                UserId = Guid.Parse("20ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.Comments.Add(comment);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);

                var cut = sut.UpdateComment(23, commentDTO);

                var actual = cut.Result;

                var actualContent = actual.Content;
                var actualUser = actual.UserId;
                var actualPost = actual.PostId;

                Assert.AreEqual(commentDTO.Content, actualContent);
            }
        }
        
        [TestMethod]
        public void FailTo_Edit_Comment_For_Post_WhenCommentNotFound()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(FailTo_Edit_Comment_For_Post_WhenCommentNotFound));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();
            var commentDTO = new CommentDTO
            {
                Id = 14,
                Content = "Comment Edited",
                PostId = 23,
                UserId = Guid.Parse("20ee8e13-16ce-4168-8c30-13e244c4497e")
            };
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);

                var cut = sut.UpdateComment(3, commentDTO);

                var actual = cut.Exception.Message;

                var expected = "One or more errors occurred. (Value cannot be null.)";
                Assert.AreEqual(expected, actual);
            }
        }
        
        [TestMethod]
        public void FailToEditComment_ForPost_WhenContentLenthIsLessThanMin()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(FailToEditComment_ForPost_WhenContentLenthIsLessThanMin));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("53ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "george@gmail.com"
            };

            var post = new Post
            {
                Id = 39,
                Content = "Test for a post"
            };
            var commentDTO = new CommentDTO
            {
                Id = 4,
                Content = "Com",
                PostId = 39,
                UserId = Guid.Parse("53ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var create = sut.AddComment(commentDTO);
                var cut = sut.UpdateComment(4, commentDTO);

                var actual = cut.Exception.Message;

                var expected = "One or more errors occurred. (Content not in range)";
                Assert.AreEqual(expected, actual);
            }
        }
        [TestMethod]
        public void FailToEditComment_ForPost_WhenContentLenthIsMoreThanMax()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(FailToEditComment_ForPost_WhenContentLenthIsMoreThanMax));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("53ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "george@gmail.com"
            };

            var post = new Post
            {
                Id = 39,
                Content = "Test for a post"
            };
            var commentDTO = new CommentDTO
            {
                Id = 6,
                Content = "Based on your input, get a random alpha numeric string. The random string generator creates a series of numbers and letters that have no pattern. These can be helpful for creating security codes." +
                "With this utility you generate a 16 character output based on your input of numbers and upper and lower case letters.Random strings can be unique.Used in computing," +
                "                a random string generator can also be called a random character string generator.This is an important tool if you want to generate a unique set of strings. The utility generates a sequence that lacks a pattern and is random." +
                "Throughout time, randomness was generated through mechanical devices such as dice, coin flips, and playing cards. A mechanical method of achieving randomness can be more time and resource consuming especially when a large number of randomized strings are needed as they could be in statistical applications.  Computational random string generators replace the traditional mechanical devices.",
                PostId = 39,
                UserId = Guid.Parse("53ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var create = sut.AddComment(commentDTO);
                var cut = sut.UpdateComment(6, commentDTO);

                var actual = cut.Exception.Message;

                var expected = "One or more errors occurred. (Content not in range)";
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
