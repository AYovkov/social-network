﻿namespace SocialNetwork.Tests.CommentsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.CommentService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Linq;

    [TestClass]
    public class GetFirstThreeComments_Should
    {
        [TestMethod]
        public void Return_CorrectCount_For_Comments()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectCount_For_Comments));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("14ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "george@gmail.com"
            };

            var post = new Post
            {
                Id = 49,
                Content = "Test for a post"
            };
            var commentOne = new CommentDTO
            {
                Id = 9,
                Content = "Complete",
                PostId = 49,
                UserId = Guid.Parse("14ee8e13-16ce-4168-8c30-13e244c4497e")
            };
            var commentTwo = new CommentDTO
            {
                Id = 10,
                Content = "Comment",
                PostId = 49,
                UserId = Guid.Parse("14ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var createOne = sut.AddComment(commentOne);
                var createTwo = sut.AddComment(commentTwo);

                var res = sut.GetFirstThreeCommentsForPost(49);

                var actual = res.Result;

                Assert.AreEqual(2, actual.Count());
            }
        }
    }
}
