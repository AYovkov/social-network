﻿namespace SocialNetwork.Tests.CommentsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.CommentService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using System;

    [TestClass]
    public class DeleteComment_Should
    {
        [TestMethod]
        public void Successfully_Delete_Comment()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Successfully_Delete_Comment));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("12ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "geori@gmail.com"
            };

            var post = new Post
            {
                Id = 39,
                Content = "Test for a post"
            };

            var comment = new Comment
            {
                Id = 12,
                Content = "Test for a comment",
                PostId = 99,
                UserId = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.Comments.Add(comment);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var result = sut.DeleteComment(12);

                var actual = result.Result;

                Assert.AreEqual(true, actual);
            }
        }
        
        [TestMethod]
        public void Return_False_When_Delete_CommentFails()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_False_When_Delete_CommentFails));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var result = sut.DeleteComment(111);

                var actual = result.Result;

                Assert.AreEqual(false, actual);
            }
        }
    }
}
