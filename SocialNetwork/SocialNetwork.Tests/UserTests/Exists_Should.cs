﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.UserService;

    [TestClass]
    public class Exists_Should
    {
        [TestMethod]
        public void CheckIfUserExists_ReturnTrue()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(CheckIfUserExists_ReturnTrue));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.Exists(firstUser.Id);

                Assert.AreEqual(true, act);
            }
        }

        [TestMethod]
        public void CheckIfUserExists_ReturnFalse()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(CheckIfUserExists_ReturnFalse));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.Exists(firstUser.Id);

                Assert.AreEqual(false, act);
            }
        }
    }
}
