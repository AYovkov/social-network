﻿namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class GetAllUsers_Should
    {
        [TestMethod]
        public void ReturnCorrectCountOfUsers_Without_SearchParameters()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectCountOfUsers_Without_SearchParameters));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllUsers(null, 5).Result;

                Assert.AreEqual(3, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(UserDTO));
            }
        }

        [TestMethod]
        public void ReturnCorrectCountOfUsers_With_SearchParameters()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectCountOfUsers_With_SearchParameters));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();

            var searchParameter = "1";

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllUsers(searchParameter, 5).Result;

                Assert.AreEqual(1, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(UserDTO));
            }
        }
    }
}
