﻿namespace SocialNetwork.Tests.UserTests
{
    using System;
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class GetAllFriends_Should
    {
        [TestMethod]
        public void GetAllFriends_CorrectCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetAllFriends_CorrectCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().First();
            var secondUserFriend = SocialNetworkUtility.GetUserFriends().Skip(1).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.UserFriends.Add(secondUserFriend);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllFriends(firstUser.Id, 5).Result;

                Assert.AreEqual(2, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(UserDTO));
            }
        }

        [TestMethod]
        public void GetAllFriends_UserDoesNotExist_ThrowNullException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetAllFriends_UserDoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var user = SocialNetworkUtility.GetUsers().First();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllFriends(user.Id));
            }
        }
    }
}
