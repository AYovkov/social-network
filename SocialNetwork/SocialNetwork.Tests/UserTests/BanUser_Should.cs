﻿namespace SocialNetwork.Tests.UserTests
{
    using System;
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.UserService;

    [TestClass]
    public class BanUser_Should
    {
        [TestMethod]
        public void DisableUser_ReturnTrue()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(DisableUser_ReturnTrue));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var user = SocialNetworkUtility.GetUsers().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.BanUser(user.Id);

                Assert.AreEqual(false, user.IsDeleted);
                Assert.AreEqual(true, assertContext.Users.First().IsDeleted);
            }
        }

        [TestMethod]
        public void DisableUser_DoesNotExist_ThrowNullException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(DisableUser_DoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var user = SocialNetworkUtility.GetUsers().First();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.BanUser(user.Id));
            }
        }
    }
}
