﻿namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.UserService;

    [TestClass]
    public class GetUsersCount_Should
    {
        [TestMethod]
        public void GetUsers_ReturnCorrectCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetUsers_ReturnCorrectCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetUsersCount();

                Assert.AreEqual(3, act);
            }
        }
    }
}
