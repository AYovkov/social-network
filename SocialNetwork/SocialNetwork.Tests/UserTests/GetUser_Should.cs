﻿namespace SocialNetwork.Tests.UserTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Data.Models;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Services.FriendRequestService;
    using Services.UserService;

    [TestClass]
    public class GetUser_Should
    {
        [TestMethod]
        public void GetUser_Should_ReturnCorrectParams()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetUser_Should_ReturnCorrectParams));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetUser(firstUser.Id).Result;

                Assert.AreEqual(firstUser.Id, act.Id);
                Assert.AreEqual(firstUser.FirstName, act.FirstName);
                Assert.AreEqual(firstUser.LastName, act.LastName);
                Assert.AreEqual(1, act.FriendsCount);
            }
        }
    }
}
