﻿namespace SocialNetwork.Web.ViewModels.FriendRequests
{
    using System;

    public class FriendRequestReceivedViewModel
    {
        public Guid SenderId { get; set; }

        public string SenderName { get; set; }
    }
}
