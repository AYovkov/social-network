﻿namespace SocialNetwork.Web.APIControllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Web.Models.FriendRequests;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FriendRequestsController : ControllerBase
    {
        private const int ReceivedRequestsPerPage = 1;

        private readonly IFriendRequestService friendRequestService;
        private readonly UserManager<User> userManager;
        private readonly IMapper mapper;

        public FriendRequestsController(IFriendRequestService friendRequestService, UserManager<User> userManager, IMapper mapper)
        {
            this.friendRequestService = friendRequestService;
            this.userManager = userManager;
            this.mapper = mapper;
        }

        [HttpPost("")]
        public async Task<IActionResult> SendFriendRequest(Guid receiverId)
        {
            var senderId = Guid.Parse(this.userManager.GetUserId(this.User));

            try
            {
                await this.friendRequestService.SendFriendRequest(senderId, receiverId);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("sent")]
        public async Task<IActionResult> SentFriendRequests()
        {
            var userId = Guid.Parse(this.userManager.GetUserId(this.User));

            try
            {
                var sentRequests = await this.friendRequestService.UserSentRequests(userId);

                return Ok(sentRequests.Select(sr => this.mapper.Map<FriendRequestViewModel>(sr)));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("received/{id:Guid}")]
        public async Task<IActionResult> ReceivedFriendRequests(Guid id, int page = 1)
        {
            var userId = Guid.Parse(this.userManager.GetUserId(this.User));

            try
            {
                var receivedRequests = await this.friendRequestService.UserReceivedRequests(userId, ReceivedRequestsPerPage, (page - 1) * ReceivedRequestsPerPage);

                var count = this.friendRequestService.GetReceivedRequestsCount(userId);

                var receivedRequestsInViewModel = new FriendRequestsReceivedViewModel
                {
                    FriendRequestReceived = receivedRequests.Select(r => this.mapper.Map<FriendRequestViewModel>(r)),
                    FriendRequestsPagesCount = (int)Math.Ceiling((double)count / ReceivedRequestsPerPage)
                };

                if (receivedRequestsInViewModel.FriendRequestsPagesCount == 0)
                {
                    receivedRequestsInViewModel.FriendRequestsPagesCount = 1;
                }

                receivedRequestsInViewModel.CurrentPage = page;

                return Ok(receivedRequestsInViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("received")]
        public async Task<IActionResult> FriendRequests(Guid senderId, string requestStatus)
        {
            var userId = Guid.Parse(this.userManager.GetUserId(this.User));

            try
            {
                await this.friendRequestService.FriendRequestAction(senderId, userId, requestStatus);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
