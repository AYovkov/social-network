﻿namespace SocialNetwork.Web.APIControllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.LikeService.Contract;
    using SocialNetwork.Web.Models;
    using System;
    using System.Threading.Tasks;

    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class LikesController : ControllerBase
    {
        private readonly ILikeService likeService;
        private readonly UserManager<User> userManager;

        public LikesController(ILikeService likeService, UserManager<User> userManager)
        {
            this.likeService = likeService;
            this.userManager = userManager;
        }

        // POST api/<LikesController>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] LikeViewModel like)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            try
            {
                await this.likeService.LikeAsync(like.PostId, user.Id);
                var likes = this.likeService.GetVotes(like.PostId);
                return likes;
            }
            catch 
            {
                return BadRequest();
            }
        }
    }
}
