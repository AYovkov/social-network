﻿namespace SocialNetwork.Web.APIControllers
{

    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Web.Models.UserFriends;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserFriendsController : ControllerBase
    {
        private readonly IUserFriendService userFriendService;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public UserFriendsController(IUserFriendService userFriendService, UserManager<User> userManager, IMapper mapper)
        {
            this.userManager = userManager;
            this.mapper = mapper;
            this.userFriendService = userFriendService;
        }

        [HttpPost("remove")]
        public async Task<IActionResult> RemoveFriend(Guid friendToRemoveId)
        {
            var localUserId = Guid.Parse(this.userManager.GetUserId(this.User));

            var result = await this.userFriendService.RemoveFriend(localUserId, friendToRemoveId);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
