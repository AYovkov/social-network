﻿namespace SocialNetwork.Web.APIControllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Services.CommentService.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService commentService;
        private readonly IMapper _mapper;

        public CommentsController(ICommentService commentService, IMapper mapper)
        {
            this.commentService = commentService;
            this._mapper = mapper;
        }

        // GET: api/Comments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CommentViewModel>>> GetComments(int id)
        {
            var comments = await this.commentService.GetAllCommentsForPost(id);


            return _mapper.Map<List<CommentViewModel>>(comments);
        }

        // PUT: api/Comments/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("update")]
        public async Task<IActionResult> PutComment([FromQuery] int id,[FromBody] CommentViewModel comment)
        {
            var postDTO = this._mapper.Map<CommentDTO>(comment);
            var postMap = await this.commentService.UpdateComment(id, postDTO);

            return Created("post", postMap);
        }

        // POST: api/Comments
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CommentViewModel>> PostComment([FromBody] CommentViewModel comment)
        {
            var postDTO = this._mapper.Map<CommentDTO>(comment);
            var postMap = await this.commentService.AddComment(postDTO);

            return Created("post", postMap);
        }

        // DELETE: api/Comments/delete?userId=""&&id=1
        [HttpDelete("delete")]
        public async Task<ActionResult<CommentViewModel>> DeleteComment(int id)
        {
            var result = await commentService.DeleteComment(id);
            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
