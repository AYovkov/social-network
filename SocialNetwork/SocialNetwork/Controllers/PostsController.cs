﻿namespace SocialNetwork.Web.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.LikeService.Contract;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.PostService.Contract;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Web.Models;
    using SocialNetwork.Web.Models.Users;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class PostsController : Controller
    {
        private const int PostsPerPage = 4;

        private readonly IWebHostEnvironment environment;
        private readonly IPhotoService photoService;
        private readonly IPostService postService;
        private readonly IUserService userService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> userManager;
        private readonly ILikeService likeService;

        public PostsController(IMapper mapper, IWebHostEnvironment environment,
            IPhotoService photoService, IPostService postService,
            UserManager<User> userManager, ILikeService likeService, IUserService userService)
        {
            this._mapper = mapper;
            this.environment = environment;
            this.photoService = photoService;
            this.postService = postService;
            this.userManager = userManager;
            this.likeService = likeService;
            this.userService = userService;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Like([FromBody] LikeViewModel like)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            try
            {
                await this.likeService.LikeAsync(like.PostId, user.Id);
                var likes = this.likeService.GetVotes(like.PostId);
                return Ok(likes);
            }
            catch
            {
                return BadRequest();
            }
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("posts/PublicPosts/{id:Guid}")]
        public async Task<IActionResult> PublicPosts(Guid id,int page = 1)
        {
            var publicPosts = await this.postService.GetAllPublicPosts(id, PostsPerPage, (page - 1) * PostsPerPage);

            var count = this.postService.GetAllPublicPostsCount(id);

            var posts = new UserPostsViewModel
            {
                Posts = publicPosts.Select(p => this._mapper.Map<PostViewModel>(p)),
                PostsPagesCount = (int)Math.Ceiling((double)count / PostsPerPage)
            };

            if (posts.PostsPagesCount == 0)
            {
                posts.PostsPagesCount = 1;
            }

            posts.CurrentPage = page;

            return PartialView("PublicPosts", posts);
        }

        [HttpGet]
        public async Task<IActionResult> AllPublicPosts(int page = 1)
        {
            var publicPosts = await this.postService.AllPublicPosts(PostsPerPage, (page - 1) * PostsPerPage);

            var count = this.postService.AllPublicPostsCount();

            var posts = new UserPostsViewModel
            {
                Posts = publicPosts.Select(p => this._mapper.Map<PostViewModel>(p)),
                PostsPagesCount = (int)Math.Ceiling((double)count / PostsPerPage)
            };

            if (posts.PostsPagesCount == 0)
            {
                posts.PostsPagesCount = 1;
            }

            posts.CurrentPage = page;

            return PartialView("AllPublicPosts",posts);
        }

        [HttpGet]
        public async Task<IActionResult> Newsfeed(int page = 1)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            var newsFeed = await this.postService.Newsfeed(user.Id, PostsPerPage, (page - 1) * PostsPerPage);

            var count =await this.postService.NewsFeedCount(user.Id);

            var newsFeedPosts = new UserPostsViewModel
            {
                Posts = newsFeed.Select(p => this._mapper.Map<PostViewModel>(p)),
                PostsPagesCount = (int)Math.Ceiling((double)count / PostsPerPage)
            };

            if (newsFeedPosts.PostsPagesCount == 0)
            {
                newsFeedPosts.PostsPagesCount = 1;
            }

            newsFeedPosts.CurrentPage = page;

            return PartialView("Newsfeed", newsFeedPosts);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetPublicPostsExceptFriendsOne(int page = 1)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            var posts = await this.postService.GetPublicPostsExceptFriendsOne(user.Id, PostsPerPage, (page - 1) * PostsPerPage);

            var count = this.postService.GetPublicPostsExceptFriendsOneCount(user.Id);

            var postsViewModel = new UserPostsViewModel
            {
                Posts = posts.Select(p => this._mapper.Map<PostViewModel>(p)),
                PostsPagesCount = (int)Math.Ceiling((double)count / PostsPerPage)
            };

            if (postsViewModel.PostsPagesCount == 0)
            {
                postsViewModel.PostsPagesCount = 1;
            }

            postsViewModel.CurrentPage = page;

            return PartialView("PublicPostsNotFriends", postsViewModel);
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Content,UserId,PhotoFile,AccessStatus,VideoUrl")] PostViewModel post)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            if (ModelState.IsValid)
            {
                post.UserId = user.Id;

                if (post.PhotoFile != null)
                {
                    PhotoDTO addedPhoto = await AddPhotoToPost(post);
                    post.PhotoName = addedPhoto.PhotoName;
                }

                var map = _mapper.Map<PostDTO>(post);
                await this.postService.AddPost(map);
                return RedirectToAction("Index","Users");
            }
            return View(post);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Edit([FromQuery] int id, [FromBody] PostViewModel post)
        {
            if (post.Content == null || post.Content.Length < 6 || post.Content.Length > 5000)
            {
                throw new ArgumentException();
            }

            if (ModelState.IsValid)
            {
                var map = _mapper.Map<PostDTO>(post);
                await this.postService.UpdatePost(id, map);

                return Ok();
            }
            return View(post);
        }

        public async Task<IActionResult> Delete(int postId)
        {
            await this.postService.DeletePost(postId);
            return Ok();
        }

        public async Task<IActionResult> ChangeStatus([FromQuery] int postId)
        {
            await this.postService.ChangeStatus(postId);

            return Ok();
        }

        private async Task<PhotoDTO> AddPhotoToPost(PostViewModel post)
        {
            var photoModel = new PhotoViewModel() { PhotoFile = post.PhotoFile };
            string wwwRootPath = environment.WebRootPath;
            string fileName = Path.GetFileNameWithoutExtension(photoModel.PhotoFile.FileName);
            string extension = Path.GetExtension(photoModel.PhotoFile.FileName);
            photoModel.PhotoName = fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;
            string path = Path.Combine(wwwRootPath + "/Images", fileName);

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await photoModel.PhotoFile.CopyToAsync(fileStream);
            }

            var addedPhoto = await this.photoService.AddPhoto(this._mapper.Map<PhotoDTO>(photoModel));
            return addedPhoto;
        }
    }
}
