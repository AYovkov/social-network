﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Data.Models;
using SocialNetwork.Services.CommentService.Contract;
using SocialNetwork.Services.EntityDTOs;
using SocialNetwork.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentService commentService;
        private readonly UserManager<User> userManager;
        private readonly IMapper mapper;
        public CommentsController(ICommentService commentService, IMapper mapper, UserManager<User> userManager)
        {
            this.commentService = commentService;
            this.mapper = mapper;
            this.userManager = userManager;
        }
        
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CommentViewModel comment)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            //var postId = int.Parse(TempData["CommentId"].ToString());
            if (ModelState.IsValid)
            {
                //comment.PostId = postId;
                comment.UserId = user.Id;
                var mapComment = this.mapper.Map<CommentDTO>(comment);
                await this.commentService.AddComment(mapComment);
                return Ok();
            }
            else
            {
                throw new ArgumentException ();
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Update([FromQuery] int id, [FromBody] CommentViewModel comment)
        {
            var postDTO = this.mapper.Map<CommentDTO>(comment);
            var postMap = await this.commentService.UpdateComment(id, postDTO);

            return Created("post", postMap);
        }

        public async Task<IActionResult> Details(int id)
        {
            var comments = await commentService.GetAllCommentsForPost(id);
            var map = this.mapper.Map<IEnumerable<CommentViewModel>>(comments);

            return View(map);
        }

        public async Task<IActionResult> Index(int id)
        {
            var comments = await commentService.GetFirstThreeCommentsForPost(id);
            var map = this.mapper.Map<IEnumerable<CommentViewModel>>(comments);

            return View(map);
        }
        public async Task<IActionResult> PublicDetails(int id)
        {
            var comments = await commentService.GetAllCommentsForPost(id);
            var map = this.mapper.Map<IEnumerable<CommentViewModel>>(comments);

            return View(map);
        }

        public async Task<IActionResult> PublicIndex(int id)
        {
            var comments = await commentService.GetFirstThreeCommentsForPost(id);
            var map = this.mapper.Map<IEnumerable<CommentViewModel>>(comments);

            return View(map);
        }


        [Authorize]
        public async Task<IActionResult> Delete([FromQuery] int commentId)
        {
            await this.commentService.DeleteComment(commentId);

            return Ok();
        }
    }
}

