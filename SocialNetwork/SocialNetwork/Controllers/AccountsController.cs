﻿namespace SocialNetwork.Web.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.UI.Services;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Web.CountriesGenerator;
    using SocialNetwork.Web.Models;
    using SocialNetwork.Web.Models.Accounts;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class AccountsController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment environment;
        private readonly IPhotoService photoService;
        private readonly IEmailSender emailSender;

        public AccountsController(SignInManager<User> signInManager, UserManager<User> userManager,
            IWebHostEnvironment environment, IPhotoService photoService, IMapper mapper, IEmailSender emailSender)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.environment = environment;
            this.photoService = photoService;
            this.emailSender = emailSender;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await this.userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            var token = await this.userManager.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = Url.Action(nameof(ResetPassword), "Accounts",
                new { token, email = user.Email }, Request.Scheme);

            var message = $"Reset your password by <a href=\"" + callbackUrl + "\">clicking here</a>.";

            await emailSender.SendEmailAsync(user.Email, "Reset Password Link", message);

            return RedirectToAction(nameof(ForgotPasswordConfirmation));
        }

        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ResetPassword(string token, string email)
        {
            var model = new ResetPasswordViewModel { Token = token, Email = email };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await this.userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                RedirectToAction(nameof(ResetPasswordConfirmation));
            }

            var resetResult = await this.userManager.ResetPasswordAsync(user, model.Token, model.Password);

            if (!resetResult.Succeeded)
            {
                foreach (var item in resetResult.Errors)
                {
                    ModelState.TryAddModelError(item.Code, item.Description);
                }

                return View();
            }

            return RedirectToAction(nameof(ResetPasswordConfirmation));
        }

        [HttpGet]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return Content("Error");
            }

            var parseUserId = Guid.TryParse(userId, out Guid id);

            if (parseUserId == false)
            {
                return Content("Error");
            }

            var user = this.userManager.Users.FirstOrDefault(u => u.Id == id);

            var result = await this.userManager.ConfirmEmailAsync(user, code);

            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }

            return View();
        }

        public async Task<IActionResult> Register()
        {
            var registerModel = new RegisterViewModel();

            ApiHelper.InitializeClient();
            var countries = await GetCountries.LoadCountries();
            ViewData["Countries"] = new SelectList(countries, "Name", "Name");
            return View(registerModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Id = Guid.NewGuid(),
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateOfBirth = model.DateOfBirth,
                    Country = model.Country,
                    Gender = model.Gender,
                    UserName = model.Email,
                    Email = model.Email,
                    CreatedOn = DateTime.Now
                };

                if (model.PhotoFile != null)
                {
                    user.PhotoName = PhotoName(model.PhotoFile);
                }
                var result = await this.userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    if (model.PhotoFile != null)
                    {
                        await AddPhotoToUser(model.PhotoFile, user.PhotoName, user.Id);
                    }
                    else
                    {
                        string wwwPath = environment.WebRootPath;
                        Directory.CreateDirectory(wwwPath + $"/images/{user.Id}");
                    }

                    var code = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail", "Accounts",
                        new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await emailSender.SendEmailAsync(user.Email, "Confirm your email",
                        $"Please confirm your account by <a href=\"" + callbackUrl + "\">clicking here</a>.");

                    await this.userManager.AddToRoleAsync(user, "User");

                    return View("ConfirmEmailInfo");
                }
            }

            return RedirectToAction("Error", "Home");
        }

        private string PhotoName(IFormFile photoFile)
        {
            var photoModel = new PhotoViewModel() { PhotoFile = photoFile };
            string fileName = Path.GetFileNameWithoutExtension(photoModel.PhotoFile.FileName);
            string extension = Path.GetExtension(photoModel.PhotoFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;

            return fileName;
        }
        private async Task AddPhotoToUser(IFormFile formFile, string photoName, Guid userId)
        {
            string wwwRootPath = environment.WebRootPath;
            Directory.CreateDirectory(wwwRootPath + $"/Images/{userId}");
            string path = Path.Combine(wwwRootPath + $"/Images/{userId}", photoName);

            var photoModel = new PhotoViewModel() { PhotoFile = formFile, PhotoName = photoName };
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await photoModel.PhotoFile.CopyToAsync(fileStream);
            }

            await this.photoService.AddPhoto(this._mapper.Map<PhotoDTO>(photoModel));
        }
    }
}
