﻿
namespace SocialNetwork.Web.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Web.Models.FriendRequests;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class FriendRequestsController : Controller
    {
        private const int ReceivedRequestsPerPage = 4;

        private readonly IFriendRequestService friendRequestService;
        private readonly UserManager<User> userManager;
        private readonly IMapper mapper;

        public FriendRequestsController(IFriendRequestService friendRequestService, IMapper mapper, UserManager<User> userManager)
        {
            this.friendRequestService = friendRequestService;
            this.userManager = userManager;
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetUserFriendRequests(Guid id, int page = 1)
        {
            var receivedRequests = await this.friendRequestService.UserReceivedRequests(id, ReceivedRequestsPerPage, (page - 1) * ReceivedRequestsPerPage);

            var count = this.friendRequestService.GetReceivedRequestsCount(id);

            var receivedRequestsInViewModel = new FriendRequestsReceivedViewModel
            {
                FriendRequestReceived = receivedRequests.Select(r => this.mapper.Map<FriendRequestViewModel>(r)),
                FriendRequestsPagesCount = (int)Math.Ceiling((double)count / ReceivedRequestsPerPage)
            };

            if (receivedRequestsInViewModel.FriendRequestsPagesCount == 0)
            {
                receivedRequestsInViewModel.FriendRequestsPagesCount = 1;
            }

            receivedRequestsInViewModel.CurrentPage = page;

            return PartialView("_SingleFriendRequestPartial", receivedRequestsInViewModel);
        }

        public IActionResult GetUserFriendRequestsCount()
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid userId);

            if (result == false)
            {
                return BadRequest();
            }

            var requestsCount = this.friendRequestService.GetReceivedRequestsCount(userId);

            return Json(requestsCount);
        }

        public async Task<IActionResult> SendFriendRequest(Guid receiverId)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid senderId);

            if (result)
            {
                await this.friendRequestService.SendFriendRequest(senderId, receiverId);

                return Content("Friend Request Sent");
            }
            else
            {
                return BadRequest();
            }
        }

        public async Task<IActionResult> FriendRequestAction(Guid senderId, string requestAction)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid receiverId);

            if (result && requestAction != null)
            {
               string actionResult = await this.friendRequestService.FriendRequestAction(senderId, receiverId, requestAction);

                return Content($"Friend Request {actionResult}");
            }
            else
            {
                return BadRequest();
            }
        }

        public async Task<IActionResult> CancelSentRequest(Guid receiverId)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid senderId);

            if (result)
            {
                await this.friendRequestService.CancelSentRequest(senderId, receiverId);

                return Content("Friend Request Canceled");
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
