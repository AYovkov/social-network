﻿namespace SocialNetwork.Web.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Web.CountriesGenerator;
    using SocialNetwork.Web.Models;
    using SocialNetwork.Web.Models.Users;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;


    public class UsersController : Controller
    {
        private const int FriendsPerPage = 4;
        private const int PostsPerPage = 5;
        private const int UsersPerPage = 5;

        private readonly IUserService userService;
        private readonly IUserFriendService userFriendService;
        private readonly IWebHostEnvironment environment;
        private readonly IPhotoService photoService;
        private readonly UserManager<User> userManager;
        private readonly IMapper mapper;

        public UsersController(IUserService userService, IMapper mapper, UserManager<User> userManager, IUserFriendService userFriendService,
             IWebHostEnvironment environment, IPhotoService photoService)
        {
            this.userService = userService;
            this.environment = environment;
            this.photoService = photoService;
            this.userFriendService = userFriendService;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid userId);

            if (result == false)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var user = await this.userService.GetUser(userId);

            var userToViewModel = this.mapper.Map<UserViewModel>(user);

            return View(userToViewModel);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("users/{id:Guid}")]
        public async Task<IActionResult> Index(Guid id)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid visitorUserId);

            if (id == visitorUserId)
            {
                return Redirect("https://localhost:5001/users");
            }

            var user = await this.userService.GetUser(id);

            var userToViewModel = this.mapper.Map<UserViewModel>(user);

            if (result)
            {
                if (id != visitorUserId)
                {
                    var areFriends = await this.userService.CheckIfFriends(id, visitorUserId);

                    if (areFriends)
                    {
                        userToViewModel.AreFriends = true;
                    }
                    else
                    {
                        var isRequestSent = await this.userService.CheckIfRequestIsSent(id, visitorUserId);

                        if (isRequestSent)
                        {
                            userToViewModel.IsRequestSent = true;
                        }
                    }
                }
            }

            return View(userToViewModel);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("users/getuserposts/{id:Guid}")]
        public async Task<IActionResult> GetUserPosts(Guid id, int page = 1)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid visitorId);

            IEnumerable<PostDTO> userPosts = await this.userService.GetAllUserPosts(id, visitorId, PostsPerPage, (page - 1) * PostsPerPage);

            var count = await this.userService.GetPostsCount(id, visitorId);

            var userPostsInViewModel = new UserPostsViewModel
            {
                Posts = userPosts.Select(p => this.mapper.Map<PostViewModel>(p)),
                PostsPagesCount = (int)Math.Ceiling((double)count / PostsPerPage)
            };

            if (userPostsInViewModel.PostsPagesCount == 0)
            {
                userPostsInViewModel.PostsPagesCount = 1;
            }

            userPostsInViewModel.CurrentPage = page;

            return PartialView("Posts", userPostsInViewModel);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("users/getuserfriends/{id:Guid?}")]
        public async Task<IActionResult> GetUserFriends(Guid id, int page = 1)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid visitorId);

            IEnumerable<UserDTO> userFriends;

            if (result)
            {
                userFriends = await this.userService.GetAllFriendsWithVisitor(id, visitorId, FriendsPerPage, (page - 1) * FriendsPerPage);
            }
            else
            {
                userFriends = await this.userService.GetAllFriends(id, FriendsPerPage, (page - 1) * FriendsPerPage);
            }

            var count = this.userService.GetFriendsCount(id, visitorId);

            var userFriendsInViewModel = new FriendsViewModel
            {
                Friends = userFriends.Select(u => this.mapper.Map<UserViewModel>(u)),
                FriendsPagesCount = (int)Math.Ceiling((double)count / FriendsPerPage)
            };

            if (userFriendsInViewModel.FriendsPagesCount == 0)
            {
                userFriendsInViewModel.FriendsPagesCount = 1;
            }

            userFriendsInViewModel.CurrentPage = page;

            return PartialView("_SingleFriendPartial", userFriendsInViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveFriend(Guid id)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid userId);

            if (result == false)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var removeFriend = await this.userFriendService.RemoveFriend(userId, id);

            if (removeFriend == false)
            {
                return BadRequest();
            }

            return Content("Friend Removed");
        }

        public async Task<IActionResult> Search(string searchParameter = null, int page = 1)
        {
            var users = await this.userService.GetAllUsers(searchParameter, UsersPerPage, (page - 1) * UsersPerPage);

            int usersCount;

            if (searchParameter != null)
            {
                usersCount = this.userService.GetUsersCountWithSearchParameter(searchParameter);
            }
            else
            {
                usersCount = this.userService.GetUsersCount();
            }

            var userSearchViewModel = new UserSearchViewModel
            {
                Users = users.Select(u => this.mapper.Map<UserViewModel>(u)),
                UsersPagesCount = (int)Math.Ceiling((double)usersCount / UsersPerPage),
                TotalUsers = usersCount,
            };

            if (searchParameter != null)
            {
                userSearchViewModel.SearchParameter = searchParameter;
            }

            if (userSearchViewModel.UsersPagesCount == 0)
            {
                userSearchViewModel.UsersPagesCount = 1;
            }

            userSearchViewModel.CurrentPage = page;

            return View(userSearchViewModel);
        }

        public async Task<IActionResult> GetUserPhotos(Guid id)
        {
            var user = await this.userService.GetUser(id);

            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid visitorId);

            bool showPictures;

            if (user.IsPrivate)
            {
                if (visitorId == id)
                {
                    showPictures = true;
                }
                else if (await this.userService.CheckIfFriends(id, visitorId))
                {
                    showPictures = true;
                }
                else
                {
                    showPictures = false;
                }
            }
            else
            {
                showPictures = true;
            }

            UserPhotosViewModel userPhotos = new UserPhotosViewModel
            {
                Id = id
            };

            if (showPictures)
            {
                string idToString = id.ToString();

                var rootDir = $"{this.environment.WebRootPath}/images/{idToString}";

                var img = Directory.EnumerateFiles(rootDir)
                    .Select(a => Path.GetRelativePath(rootDir, a))
                    .Select(a => a.Replace("\\", "/"));

                userPhotos.Photos = img;
            }
            else
            {
                userPhotos.Photos = Enumerable.Empty<string>();
            }

            return PartialView("_SinglePhotoPartial", userPhotos);
        }

        public async Task<IActionResult> GetUserLatestPhotos(Guid id)
        {
            var user = await this.userService.GetUser(id);

            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid visitorId);

            bool showPictures;

            if (user.IsPrivate)
            {
                if (visitorId == id)
                {
                    showPictures = true;
                }
                else if (await this.userService.CheckIfFriends(id, visitorId))
                {
                    showPictures = true;
                }
                else
                {
                    showPictures = false;
                }
            }
            else
            {
                showPictures = true;
            }

            UserPhotosViewModel userPhotos = new UserPhotosViewModel
            {
                Id = id
            };

            IEnumerable<string> img = Enumerable.Empty<string>();

            if (showPictures)
            {
                string idToString = id.ToString();

                var rootDir = $"{this.environment.WebRootPath}/images/{idToString}";

                img = Directory.EnumerateFiles(rootDir)
               .Select(a => Path.GetRelativePath(rootDir, a))
               .Select(a => a.Replace("\\", "/"))
               .Take(4);
            }

            userPhotos.Photos = img;

            return PartialView("_SingleLatestPhotosPartial", userPhotos);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Ban([FromQuery] Guid userId)
        {
            await this.userService.BanUser(userId);

            return Content("User account has been locked");
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Unban([FromQuery] Guid userId)
        {
            await this.userService.UnbanUser(userId);

            return Content("User account has been unlocked");
        }

        public async Task<IActionResult> Edit()
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid userId);

            if (result == false)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var user = await this.userService.GetUser(userId);

            var userToViewModel = this.mapper.Map<UserInputViewModel>(user);

            ApiHelper.InitializeClient();
            var countries = await GetCountries.LoadCountries();
            ViewData["Countries"] = new SelectList(countries, "Name", "Name");

            return View(userToViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserInputViewModel model)
        {
            var result = Guid.TryParse(this.userManager.GetUserId(this.User), out Guid userId);

            if (result == false)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            if (ModelState.IsValid)
            {
                var userModelToDto = this.mapper.Map<UserDTO>(model);

                if (model.PhotoFile != null)
                {
                    userModelToDto.PhotoName = PhotoName(model.PhotoFile);
                    await AddPhotoToUser(model.PhotoFile, userModelToDto.PhotoName, userId);
                }

                await this.userService.Edit(userId, userModelToDto);

                return RedirectToAction("Edit");
            }

            return View(model);
        }

        private string PhotoName(IFormFile photoFile)
        {
            var photoModel = new PhotoViewModel() { PhotoFile = photoFile };
            string fileName = Path.GetFileNameWithoutExtension(photoModel.PhotoFile.FileName);
            string extension = Path.GetExtension(photoModel.PhotoFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssff") + extension;

            return fileName;
        }
        private async Task AddPhotoToUser(IFormFile formFile, string photoName, Guid userId)
        {
            string wwwRootPath = environment.WebRootPath;
            Directory.CreateDirectory(wwwRootPath + $"/Images/{userId}");
            string path = Path.Combine(wwwRootPath + $"/Images/{userId}", photoName);

            var photoModel = new PhotoViewModel() { PhotoFile = formFile, PhotoName = photoName };
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await photoModel.PhotoFile.CopyToAsync(fileStream);
            }

            await this.photoService.AddPhoto(this.mapper.Map<PhotoDTO>(photoModel));
        }
    }
}
