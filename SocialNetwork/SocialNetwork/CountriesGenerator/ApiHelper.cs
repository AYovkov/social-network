﻿namespace SocialNetwork.Web.CountriesGenerator
{
    using System.Net.Http;
    using System.Net.Http.Headers;


    public class ApiHelper
    {
        public static HttpClient APIClient { get; set; }

        public static void InitializeClient()
        {
            APIClient = new HttpClient();
            APIClient.DefaultRequestHeaders.Accept.Clear();
            APIClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
