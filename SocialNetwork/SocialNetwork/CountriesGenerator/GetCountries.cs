﻿namespace SocialNetwork.Web.CountriesGenerator
{
    using SocialNetwork.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class GetCountries
    {
        public static async Task<List<CountryViewModel>> LoadCountries()
        {
            string url = "https://restcountries.eu/rest/v2/all?fields=name";

            using (HttpResponseMessage response = await ApiHelper.APIClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<CountryViewModel> countries = await response.Content.ReadAsAsync<List<CountryViewModel>>();

                    return countries;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
