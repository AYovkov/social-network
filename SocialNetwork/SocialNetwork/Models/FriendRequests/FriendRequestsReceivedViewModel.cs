﻿namespace SocialNetwork.Web.Models.FriendRequests
{
    using SocialNetwork.Web.Models.Users;
    using System.Collections.Generic;

    public class FriendRequestsReceivedViewModel
    {
        public IEnumerable<FriendRequestViewModel> FriendRequestReceived { get; set; }
        public int CurrentPage { get; set; }
        public int FriendRequestsPagesCount { get; set; }
    }
}
