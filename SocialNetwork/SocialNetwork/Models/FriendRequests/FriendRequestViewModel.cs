﻿namespace SocialNetwork.Web.Models.FriendRequests
{
    using System;

    public class FriendRequestViewModel
    {
        public Guid SenderId { get; set; }

        public string SenderName { get; set; }

        public string SenderPhotoName { get; set; }

        public Guid ReceiverId { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiverPhotoName { get; set; }
    }
}
