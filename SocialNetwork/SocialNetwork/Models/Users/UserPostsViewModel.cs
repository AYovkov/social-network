﻿namespace SocialNetwork.Web.Models.Users
{
    using System.Collections.Generic;

    public class UserPostsViewModel
    {
        public IEnumerable<PostViewModel> Posts { get; set; }
        public int CurrentPage { get; set; }
        public int PostsPagesCount { get; set; }
    }
}
