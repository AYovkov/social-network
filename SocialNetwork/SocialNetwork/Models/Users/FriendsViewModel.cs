﻿namespace SocialNetwork.Web.Models.Users
{
    using System.Collections.Generic;
    using System.Text.Json.Serialization;

    public class FriendsViewModel
    {
        public IEnumerable<UserViewModel> Friends { get; set; }

        [JsonIgnore]
        public int CurrentPage { get; set; }
        [JsonIgnore]
        public int FriendsPagesCount { get; set; }
    }
}
