﻿namespace SocialNetwork.Web.Models.Users
{
    using System.Collections.Generic;

    public class UserSearchViewModel
    {
        public IEnumerable<UserViewModel> Users { get; set; }
        public int CurrentPage { get; set; }
        public int UsersPagesCount { get; set; }
        public int TotalUsers { get; set; }
        public string SearchParameter { get; set; }
    }
}
