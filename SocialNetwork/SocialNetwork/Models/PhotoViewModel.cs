﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class PhotoViewModel
    {
        public int Id { get; set; }
        public string PhotoName { get; set; }
        public IFormFile PhotoFile { get; set; }
    }
}
