﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function refreshPage() {
    window.location.reload();
}


function getPublicComments(postId) {
    var json = { postId: postId }
    var url = "https://localhost:5001/comments/publicdetails/" + postId;
    $.ajax({
        type: "GET",
        url: url,
        data: JSON.stringify(json),
        success: function (data) {
            $('#loadPublicComments_' + postId).html(data);
        }
    });
};
$('.btnCreatePost').click(function () {

    var url = $('#createPostModal').data('url');
    $.get(url, function (data) {
        $("#createPostModal").html(data);
        $('#createPostModal').modal('show');
    })
});


function addComment(postId) {
    var content = document.getElementById("contentText_" + postId).value;
    var json = {
        "content": content,
        "postId": postId
    }
    $.ajax({
        type: "Post",
        url: "https://localhost:5001/comments/create",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        processData: json,
        success: function () {
            location.reload();
        },
        statusCode: {
            500: function () {
                alert("Invalid comment content");
            }
        }
    })
};

function updatePost(id) {
    var content = document.getElementById("postContent_" + id).value;
    var json = {
        id: id,
        content: content
    };
    $.ajax({
        type: "Post",
        url: "https://localhost:5001/Posts/edit?id=" + id,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        processData: json,
        success: function () {
            location.reload();
        },
        statusCode: {
            500: function () {
                alert("Invalid content");
            }
        }
    })
}

function updateStatus(id) {
    $.ajax({
        type: "GET",
        url: "https://localhost:5001/posts/ChangeStatus?postId="+ id,
        success: function () {
            location.reload();
        }
    });
}

function updateComment(id) {
    var content = document.getElementById("editComment_" + id).value;
    var json = {
        id: id,
        content: content
    };
    $.ajax({
        type: "Post",
        url: "https://localhost:5001/Comments/update?id=" + id,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        processData: json,
        success: function () {
            location.reload();
        },
        statusCode: {
            500: function () {
                alert("Invalid comment content");
            }
        }
    })
}
function deletePost(postId) {
    $.ajax({
        type: "GET",
        url: "https://localhost:5001/Posts/Delete?postId=" + postId,
        success: function () {
            location.reload();
        }
    });
};

function deleteComment(commentId) {
    $.ajax({
        type: "GET",
        url: "https://localhost:5001/Comments/Delete?commentId=" + commentId,
        success: function () {
            location.reload();
        }
    });
};

function createForm(name) {
    var actionName = { name: name };
    $.ajax({
        type: "GET",
        url: "https://localhost:5001/posts/create",
        contentType: "application/json; charset=utf-8",
        data: actionName,
        processData: actionName,
        success: function (data) {
            $('#loadForm').html(data);
        }
    });
};


function getComments(postId) {
    var json = { postId: postId }
    var url = "https://localhost:5001/comments/details/" + postId;
    $.ajax({
        type: "GET",
        url: url,
        data: JSON.stringify(json),
        success: function (data) {
            $('#loadComments_' + postId).html(data);
        }
    });
};

function upVote(postId) {
    var json = { postId: postId };
    $.ajax({
        url: "https://localhost:5001/posts/like",
        type: "Post",
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        processData: "json",
        success: function (data) {
            console.log(data);
            $("#likeCount_" + postId).html(data);
        }
    })
};

function GetData() {
    _postsIncallback = true;
    $.ajax({
        type: 'GET',
        url: '/Users/GetUserPosts/' + userId,
        data: {
            "page": postsPageIndex
        },
        dataType: 'html',
        success: function (data) {
            if (data != null) {
                $("#posts").append(data);
                postsPageIndex++;
            }
        },
        beforeSend: function () {
            $("#loadMore").show();
        },
        complete: function () {
            $("#loadMore").hide();
            _postsIncallback = false;
        },
        error: function () {
            _postsIncallback = false;
        }
    });
};

function GetPublicPostsNoFriendsData() {
    _allPublicNoFriendsIncallback = true;
    $.ajax({
        type: 'GET',
        url: '/posts/GetPublicPostsExceptFriendsOne/',
        data: {
            "page": allPublicNoFriendsPageIndex
        },
        dataType: 'html',
        success: function (data) {
            if (data != null) {
                $("#postsPublic").append(data);
                allPublicNoFriendsPageIndex++;
            }
        },
        beforeSend: function () {
            $("#loadMore").show();
        },
        complete: function () {
            $("#loadMore").hide();
            _allPublicNoFriendsIncallback = false;
        },
        error: function () {
            _allPublicNoFriendsIncallback = false;
        }
    });
};

function GetPublicData() {
    _publicPostsIncallback = true;
    $.ajax({
        type: 'GET',
        url: '/Posts/PublicPosts/' + userId,
        data: {
            "page": publicPostsPageIndex
        },
        dataType: 'html',
        success: function (data) {
            if (data != null) {
                $("#publicPosts").append(data);
                publicPostsPageIndex++;
            }
        },
        beforeSend: function () {
            $("#loadMore").show();
        },
        complete: function () {
            $("#loadMore").hide();
            _publicPostsIncallback = false;
        },
        error: function () {
            _publicPostsIncallback = false;
        }
    });
};

function AllPublicData() {
    _allPublicIncallback = true;
    $.ajax({
        type: 'GET',
        url: '/Posts/AllPublicPosts/',
        data: {
            "page": allPublicPageIndex
        },
        dataType: 'html',
        success: function (data) {
            if (data != null) {
                $("#publicPosts").append(data);
                allPublicPageIndex++;
            }
        },
        beforeSend: function () {
            $("#loadMore").show();
        },
        complete: function () {
            $("#loadMore").hide();
            _allPublicIncallback = false;
        },
        error: function () {
            _allPublicIncallback = false;
        }
    });
};

function GetNewsFeedData() {
    _newsFeedIncallback = true;
    $.ajax({
        type: 'GET',
        url: '/Posts/newsFeed/',
        data: {
            "page": newsFeedPageIndex
        },
        dataType: 'html',
        success: function (data) {
            if (data != null) {
                $("#newsFeed").append(data);
                newsFeedPageIndex++;
            }
        },
        beforeSend: function () {
            $("#loadMore").show();
        },
        complete: function () {
            $("#loadMore").hide();
            _newsFeedIncallback = false;
        },
        error: function () {
            _newsFeedIncallback = false;
        }
    });
}
