﻿using AutoMapper;
using SocialNetwork.Data.Models;
using SocialNetwork.Services.EntityDTOs;
using SocialNetwork.Web.Models;
using SocialNetwork.Web.Models.FriendRequests;
using SocialNetwork.Web.Models.UserFriends;
using SocialNetwork.Web.Models.Users;
using System.Linq;

namespace SocialNetwork.Web.MappingProfiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PostDTO, PostViewModel>()
                .ForMember(dest => dest.Comments, opt
                => opt.MapFrom(src => src.Comments.Select(comment
                => new CommentDTO { Id=comment.Id, Content = comment.Content,PostId=comment.PostId,
                    Username = comment.Username,DeletedOn = comment.DeletedOn })))
                .ForMember(dest => dest.Likes, opt
                => opt.MapFrom(src => src.Likes.Select(like
                => new LikeDTO { LikeType = like.LikeType })))
                .ForMember(dest => dest.PhotoName, opt
                 => opt.MapFrom(src => src.PhotoName))
                .ForMember(dest => dest.Count, opt
                 => opt.MapFrom(src => src.Likes.Sum(v=>(int)v.LikeType))).ReverseMap();

            CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.Comments, opt
                => opt.MapFrom(src => src.Comments.Select(comment
                => new Comment { Id = comment.Id, PostId = comment.PostId, Content = comment.Content, DeletedOn = comment.DeletedOn })))
                .ForMember(dest => dest.PhotoName, opt
                 => opt.MapFrom(src => src.Photo.PhotoName))
                .ForMember(dest => dest.CommentsCount, opt
                 => opt.MapFrom(src => src.Comments.Where(c => !c.IsDeleted).Count()))
                .ForMember(dest => dest.Username, opt
                 => opt.MapFrom(src => $"{src.User.FirstName} {src.User.LastName}")).ReverseMap();

            CreateMap<PostDTO, UserPostsViewModel>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, UserViewModel>().ReverseMap();

            CreateMap<UserDTO, UserInputViewModel>().ReverseMap();

            CreateMap<FriendRequest, FriendRequestReceivedDTO>()
                .ForMember(dest => dest.SenderName, opt => opt.MapFrom(fn => $"{fn.Sender.FirstName} {fn.Sender.LastName}"));
            CreateMap<FriendRequest, FriendRequestSentDTO>()
                .ForMember(dest => dest.ReceiverName, opt => opt.MapFrom(fn => $"{fn.Receiver.FirstName} {fn.Receiver.LastName}"));

            CreateMap<FriendRequestReceivedDTO, FriendRequestViewModel>();

            CreateMap<UserFriend, UserFriendDTO>();
            CreateMap<UserFriendDTO, UserFriendViewModel>();

            CreateMap<Like, LikeDTO>().ReverseMap();
            CreateMap<LikeDTO, LikeViewModel>().ReverseMap();

            CreateMap<Comment, CommentDTO>()
                .ForMember(dest => dest.Username, opt =>
                opt.MapFrom(src => $"{src.User.FirstName} {src.User.LastName}")).ReverseMap();
            CreateMap<CommentDTO, CommentViewModel>()
                .ForMember(dest => dest.Username, opt =>
                opt.MapFrom(src => src.Username)).ReverseMap();

            CreateMap<Photo, PhotoDTO>().ReverseMap();
            CreateMap<PhotoDTO, PhotoViewModel>().ReverseMap();
        }
    }
}
