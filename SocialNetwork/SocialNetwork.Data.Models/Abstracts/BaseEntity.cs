﻿namespace SocialNetwork.Data.Models.Abstracts
{
    using System;
    using Contracts;

    public abstract class BaseEntity : IEntity
    {
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
