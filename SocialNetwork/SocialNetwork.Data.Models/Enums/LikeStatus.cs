﻿namespace SocialNetwork.Data.Models.Enums
{
    public enum LikeStatus
    {
        Neutral = 0,
        UpLike = 1,
    }
}