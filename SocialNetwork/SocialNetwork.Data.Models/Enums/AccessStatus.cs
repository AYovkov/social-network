﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SocialNetwork.Data.Models.Enums
{
    public enum AccessStatus
    {
        Public = 0,
        Private = 1
    }
}
