﻿namespace SocialNetwork.Data.Models
{
    using System;
    using Enums;

    public class Like
    {
        public int PostId { get; set; }

        public virtual Post Post { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }

        public LikeStatus LikeType { get; set; }
    }
}