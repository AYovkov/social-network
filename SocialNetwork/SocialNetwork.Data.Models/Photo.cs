﻿namespace SocialNetwork.Data.Models
{
    using Abstracts;
    using Microsoft.AspNetCore.Http;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Photo 
    {
        public int Id { get; set; }
        [Required]
        [DisplayName("Photo name")]
        public string PhotoName { get; set; }

        [NotMapped]
        [DisplayName("Upload photo")]
        public IFormFile PhotoFile { get; set; }
    }
}
