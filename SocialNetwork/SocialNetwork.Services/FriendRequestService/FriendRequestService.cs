﻿namespace SocialNetwork.Services.FriendRequestService
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class FriendRequestService : IFriendRequestService
    {
        private readonly SocialNetworkDbContext context;
        private readonly IUserService userService;
        private readonly IUserFriendService _userFriendService;
        private readonly IMapper mapper;

        public FriendRequestService(SocialNetworkDbContext context, IUserService userService,
            IUserFriendService userFriendService, IMapper mapper)
        {
            this.context = context;
            this.userService = userService;
            this._userFriendService = userFriendService;
            this.mapper = mapper;
        }

        public async Task<string> FriendRequestAction(Guid senderId, Guid receiverId, string requestStatus)
        {
            string action = String.Empty;

            if (requestStatus == "accept")
            {
                await this.AcceptFriendRequest(senderId, receiverId);
                action = "Accepted";
            }
            else if (requestStatus == "decline")
            {
                await this.DeclineFriendRequest(senderId, receiverId);
                action = "Declined";
            }

            return action;
        }

        public async Task CancelSentRequest(Guid senderId, Guid receiverId)
        {
            if (!this.userService.Exists(senderId) || !this.userService.Exists(receiverId))
            {
                throw new ArgumentNullException();
            }

            await this.DeclineFriendRequest(senderId, receiverId);
        }

        public async Task SendFriendRequest(Guid senderId, Guid receiverId)
        {
            if (this.Exists(senderId, receiverId)
                || this.Exists(receiverId, senderId)
                || !this.userService.Exists(senderId)
                || !this.userService.Exists(receiverId))
            {
                throw new ArgumentNullException();
            }

            var friendRequest = new FriendRequest
            {
                SenderId = senderId,
                ReceiverId = receiverId,
                FriendRequestStatus = FriendRequestStatus.Pending
            };

            await this.context.FriendRequests.AddAsync(friendRequest);
            await this.context.SaveChangesAsync();
        }

        public bool Exists(Guid senderId, Guid receiverId)
        {
            return this.context.FriendRequests.Any(fr => fr.SenderId == senderId && fr.ReceiverId == receiverId);
        }

        public async Task<IEnumerable<FriendRequestReceivedDTO>> UserReceivedRequests(Guid id, int? take = null, int skip = 0)
        {
            var friendRequests = this.context.FriendRequests
                .Include(fr => fr.Sender)
                .Where(u => u.ReceiverId == id && u.FriendRequestStatus != FriendRequestStatus.Accepted)
                .Skip(skip);

            if (take.HasValue)
            {
                friendRequests = friendRequests.Take(take.Value);
            }

            var friendRequestsToDto = await friendRequests.Select(fr => this.mapper.Map<FriendRequestReceivedDTO>(fr)).ToListAsync();

            return friendRequestsToDto;
        }

        public int GetReceivedRequestsCount(Guid id)
        {
            return this.context.FriendRequests.Where(u => u.ReceiverId == id && u.FriendRequestStatus != FriendRequestStatus.Accepted).Count();
        }

        public async Task<IEnumerable<FriendRequestSentDTO>> UserSentRequests(Guid id)
        {
            var user = await this.context.Users
                .Include(fs => fs.FriendRequestSent)
                .ThenInclude(r => r.Receiver)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var userSentRequests = this.mapper.Map<List<FriendRequestSentDTO>>(user.FriendRequestSent);

            return userSentRequests;
        }

        private async Task AcceptFriendRequest(Guid senderId, Guid receiverId)
        {
            var friendRequest = await this.context.FriendRequests.FirstOrDefaultAsync(friendRequest =>
            friendRequest.SenderId == senderId && friendRequest.ReceiverId == receiverId);

            var areFriends = await this.userService.CheckIfFriends(senderId, receiverId);

            if (friendRequest == null || areFriends)
            {
                throw new ArgumentNullException();
            }

            friendRequest.FriendRequestStatus = FriendRequestStatus.Accepted;

            await this._userFriendService.AddFriend(senderId, receiverId);
            await this.context.SaveChangesAsync();
        }

        private async Task DeclineFriendRequest(Guid senderId, Guid receiverId)
        {
            var friendRequest = await this.context.FriendRequests.FirstOrDefaultAsync(fr =>
            fr.SenderId == senderId && fr.ReceiverId == receiverId);

            if (friendRequest == null)
            {
                throw new ArgumentNullException();
            }

            friendRequest.FriendRequestStatus = FriendRequestStatus.Declined;

            this.context.FriendRequests.Remove(friendRequest);
            this.context.SaveChanges();
        }
    }
}
