﻿namespace SocialNetwork.Services.EntityDTOs
{
    using SocialNetwork.Data.Models;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.Contracts;
    using System;
    using System.Collections.Generic;

    public class PostDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public AccessStatus AccessStatus { get; set; }
        public int? PhotoId { get; set; }
        public string PhotoName { get; set; }
        public string Username { get; set; }
        public Guid UserId { get; set; }
        public string VideoUrl { get; set; }
        public int CommentsCount { get; set; }
        public virtual UserDTO User { get; set; }
        public virtual ICollection<CommentDTO> Comments { get; set; }
        public virtual ICollection<LikeDTO> Likes { get; set; }
        public DateTime CreatedOn { get ; set ; }
        public bool IsDeleted { get ; set ; }
        public DateTime? DeletedOn { get ; set ; }
    }
}