﻿namespace SocialNetwork.Services.UserService
{
    using Microsoft.AspNetCore.Http;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IUserService
    {
        Task<UserDTO> Edit(Guid id, UserDTO userDTO);
        //void AddProfilePicture(IFormFile photo, Guid userId);
        Task<IEnumerable<UserDTO>> GetAllUsers(string searchParameter = null, int? take = null, int skip = 0);
        Task<IEnumerable<UserDTO>> GetAllFriends(Guid id, int? take = null, int skip = 0);
        Task<IEnumerable<UserDTO>> GetAllFriendsWithVisitor(Guid userId, Guid visitorId, int? take = null, int skip = 0);
        Task<IEnumerable<PostDTO>> GetAllUserPosts(Guid id, Guid? visitorId, int? take = null, int skip = 0);
        Task<UserDTO> GetUser(Guid id);
        Task BanUser(Guid id);
        Task UnbanUser(Guid id);
        int GetFriendsCount(Guid id, Guid? visitorId);
        Task<int> GetPostsCount(Guid id, Guid? visitorId);
        int GetUsersCount();
        int GetUsersCountWithSearchParameter(string searchParameter);
        bool Exists(Guid id);
        Task<bool> CheckIfFriends(Guid userId, Guid secondUserId);
        Task<bool> CheckIfRequestIsSent(Guid userId, Guid secondUserId);
    }
}
