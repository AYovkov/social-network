﻿using SocialNetwork.Services.DTOsProvider.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOsProvider
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
