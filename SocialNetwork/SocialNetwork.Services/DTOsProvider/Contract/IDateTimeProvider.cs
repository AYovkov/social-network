﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOsProvider.Contract
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
