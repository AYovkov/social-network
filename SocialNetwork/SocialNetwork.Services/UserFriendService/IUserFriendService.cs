﻿using SocialNetwork.Services.EntityDTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetwork.Services.UserFriend
{
    public interface IUserFriendService
    { 
        Task AddFriend(Guid senderId, Guid receiverId);
        Task<bool> RemoveFriend(Guid localUserId, Guid friendToRemoveId);
    }
}
