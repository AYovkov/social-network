﻿namespace SocialNetwork.Services.PhotoService
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using System.Threading.Tasks;

    public class PhotoService : IPhotoService
    {
        private readonly SocialNetworkDbContext context;
        private readonly IMapper _mapper;

        public PhotoService(SocialNetworkDbContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }

        public async Task<PhotoDTO> AddPhoto(PhotoDTO photo)
        {
            var map = _mapper.Map<Photo>(photo);
            await this.context.Photos.AddAsync(map);
            await this.context.SaveChangesAsync();

            return photo;
        }

        public async Task<bool> DeletePhoto(int id)
        {
            try
            {
                var photo = await this.context.Photos.FirstOrDefaultAsync(photo => photo.Id == id);
                var map = _mapper.Map<Photo>(photo);
                this.context.Photos.Remove(map);

                await this.context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<PhotoDTO> GetPhoto(int id)
        {
            var photo = await context.Photos
                .FirstOrDefaultAsync(m => m.Id == id);

            var photoDTO = this._mapper.Map<PhotoDTO>(photo);

            return photoDTO;
        }
    }
}
