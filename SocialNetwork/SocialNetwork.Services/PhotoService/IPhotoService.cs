﻿namespace SocialNetwork.Services.PhotoService
{
    using SocialNetwork.Services.EntityDTOs;
    using System.Threading.Tasks;

    public interface IPhotoService
    {
        Task<PhotoDTO> AddPhoto(PhotoDTO photo);
        Task<PhotoDTO> GetPhoto(int id);
        Task<bool> DeletePhoto(int id);
    }
}
