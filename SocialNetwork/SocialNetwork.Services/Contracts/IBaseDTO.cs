﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.Contracts
{
    public interface IBaseDTO
    {
        DateTime CreatedOn { get; set; }
        bool IsDeleted { get; set; }
        DateTime? DeletedOn { get; set; }
    }
}
