﻿
namespace SocialNetwork.Services.PostService
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PostService.Contract;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class PostService : IPostService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly SocialNetworkDbContext dbContext;
        private readonly IMapper _mapper;
        private readonly IUserService userService;

        public PostService(SocialNetworkDbContext dbContext, IMapper mapper
            , IDateTimeProvider dateTimeProvider, IUserService userService)
        {
            this.dbContext = dbContext;
            this._mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
            this.userService = userService;
        }

        public async Task<PostDTO> AddPost(PostDTO postDTO)
        {
            if (postDTO.PhotoName != null)
            {
                var photo = await this.dbContext.Photos
                    .FirstOrDefaultAsync(p => p.PhotoName == postDTO.PhotoName);

                postDTO.PhotoId = photo.Id;
            }
            postDTO.CreatedOn = dateTimeProvider.GetDateTime();
            postDTO.IsDeleted = false;
            postDTO.VideoUrl = CheckForVideo(postDTO);
            var map = _mapper.Map<Post>(postDTO);
            if (map.PhotoId == null)
            {
                map.Photo = null;
            }
            await this.dbContext.Posts.AddAsync(map);
            await this.dbContext.SaveChangesAsync();

            return postDTO;
        }

        private static string CheckForVideo(PostDTO postDTO)
        {
            if (postDTO.VideoUrl != null)
            {
                var url = postDTO.VideoUrl.Split("v=")[1];
                postDTO.VideoUrl = "https://www.youtube.com/embed/" + url.Split('&')[0];
            }
            return postDTO.VideoUrl;
        }

        public async Task<bool> DeletePost(int id)
        {
            try
            {
                var post = await this.dbContext.Posts
                .Where(p => !p.IsDeleted)
                .FirstOrDefaultAsync(p => p.Id == id);

                post.IsDeleted = true;
                post.DeletedOn = dateTimeProvider.GetDateTime();

                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<PostDTO> GetPost(int id)
        {
            var post = await this.dbContext.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(post => !post.IsDeleted)
                .FirstOrDefaultAsync(post => post.Id == id);

            if (post == null)
            {
                throw new ArgumentNullException();
            }

            var postDTO = _mapper.Map<PostDTO>(post);

            return postDTO;
        }

        public async Task<PostDTO> UpdatePost(int id, PostDTO postDTO)
        {
            var post = await this.dbContext.Posts
                .Where(p => !p.IsDeleted)
                .FirstOrDefaultAsync(p => p.Id == id);
            if (post == null)
            {
                throw new ArgumentNullException();
            }

            post.Content = postDTO.Content;

            await this.dbContext.SaveChangesAsync();

            return postDTO;
        }

        public async Task<IEnumerable<PostDTO>> GetPostsForUsers(Guid id)
        {
            var posts = await this.dbContext.Posts
               .Include(post => post.Photo)
               .Include(post => post.User)
               .Include(post => post.Comments)
               .Include(post => post.Likes)
               .Where(post => !post.IsDeleted && post.UserId == id)
               .Select(post => _mapper.Map<PostDTO>(post))
               .ToListAsync();

            return posts.OrderByDescending(p => p.CreatedOn);
        }

        public async Task ChangeStatus(int id)
        {
            var post = await this.dbContext.Posts
                .Where(p => !p.IsDeleted)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (post.AccessStatus == AccessStatus.Private)
            {
                post.AccessStatus = AccessStatus.Public;
            }
            else
            {
                post.AccessStatus = AccessStatus.Private;
            }

            await this.dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<PostDTO>> AllPublicPosts(int? take = null, int skip = 0)
        {
            var posts = this.dbContext.Posts
               .Include(post => post.Photo)
               .Include(post => post.User)
               .Include(post => post.Comments)
               .Include(post => post.Likes)
               .Where(p => !p.IsDeleted && p.AccessStatus == AccessStatus.Public)
               .OrderByDescending(p => p.CreatedOn)
               .Skip(skip);

            if (take.HasValue)
            {
                posts = posts.Take(take.Value);
            }

            var postsDto = await posts.Select(p => this._mapper.Map<PostDTO>(p)).ToListAsync();

            return postsDto;
        }

        public async Task<IEnumerable<PostDTO>> GetPublicPostsExceptFriendsOne(Guid id, int? take = null, int skip = 0)
        {
            var friendsIds = this.userService.GetAllFriends(id).Result.Select(u => u.Id);

            var posts = this.dbContext.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(p => !p.IsDeleted && p.AccessStatus == AccessStatus.Public && friendsIds.All(i => i != p.UserId) && p.UserId != id)
                .OrderByDescending(p => p.CreatedOn)
                .Skip(skip);

            if (take.HasValue)
            {
                posts = posts.Take(take.Value);
            }

            var postsDto = await posts.Select(p => this._mapper.Map<PostDTO>(p)).ToListAsync();

            return postsDto;
        }

        public int GetPublicPostsExceptFriendsOneCount(Guid id)
        {
            var friendsIds = this.userService.GetAllFriends(id).Result.Select(u => u.Id);

            return this.dbContext.Posts
                .Count(p => !p.IsDeleted && p.AccessStatus == AccessStatus.Public &&
                            friendsIds.All(i => i != p.UserId) && p.UserId != id);
        }

        public int AllPublicPostsCount()
        {
            return this.dbContext.Posts.Where(p => !p.IsDeleted && p.AccessStatus == AccessStatus.Public).Count();
        }

        public async Task<IEnumerable<PostDTO>> Newsfeed(Guid id, int? take = null, int skip = 0)
        {
            List<PostDTO> p = await FriendsPosts(id);

            var posts = p.OrderByDescending(p => p.CreatedOn).Skip(skip);

            if (take.HasValue)
            {
                posts = posts.Take(take.Value);
            }

            return posts;
        }

        private async Task<List<PostDTO>> FriendsPosts(Guid id)
        {
            var friends = await this.userService.GetAllFriends(id);
            var p = new List<PostDTO>();
            foreach (var friend in friends)
            {
                foreach (var item in await GetPostsForUsers(friend.Id))
                {
                    p.Add(item);
                }
            }

            return p;
        }

        public async Task<int> NewsFeedCount(Guid id)
        {
            var friendsPosts = await FriendsPosts(id);

            return friendsPosts.Count();
        }


        public async Task<IEnumerable<PostDTO>> GetAllPublicPosts(Guid id, int? take = null, int skip = 0)
        {
            var posts = this.dbContext.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(post => !post.IsDeleted && post.AccessStatus == AccessStatus.Public && post.UserId == id)
                .OrderByDescending(p => p.CreatedOn).Skip(skip);

            if (take.HasValue)
            {
                posts = posts.Take(take.Value);
            }

            var postsDto = await posts.Select(p => this._mapper.Map<PostDTO>(p)).ToListAsync();

            return postsDto;
        }
        public int GetAllPublicPostsCount(Guid id)
        {

            var count = this.dbContext.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(post => !post.IsDeleted && post.AccessStatus == AccessStatus.Public && post.UserId == id).Count();

            return count;
        }
    }
}
