﻿namespace SocialNetwork.Services.PostService.Contract
{
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPostService
    {
        Task<PostDTO> GetPost(int id);
        Task ChangeStatus(int id);
        Task<IEnumerable<PostDTO>> GetPostsForUsers(Guid id);
        Task<PostDTO> AddPost(PostDTO postDTO);
        Task<PostDTO> UpdatePost(int id, PostDTO postDTO);
        Task<bool> DeletePost(int id);
        Task<IEnumerable<PostDTO>> Newsfeed(Guid id, int? take = null, int skip = 0);
        Task<int> NewsFeedCount(Guid id);
        Task<IEnumerable<PostDTO>> AllPublicPosts(int? take = null, int skip = 0);
        int AllPublicPostsCount();
        Task<IEnumerable<PostDTO>> GetAllPublicPosts(Guid id, int? take = null, int skip = 0);
        Task<IEnumerable<PostDTO>> GetPublicPostsExceptFriendsOne(Guid id, int? take = null, int skip = 0);
        int GetPublicPostsExceptFriendsOneCount(Guid id);
        int GetAllPublicPostsCount(Guid id);
    }
}
